int sensor = 0;      
int valorSensor = 0;

const int buzzer = 10;

void setup(){
  
  Serial.begin(9600);
   
  pinMode(buzzer,OUTPUT);
  
}

void loop(){
   
  int valorSensor = analogRead(sensor);
 
  if (valorSensor < 100) {
    tone(buzzer, 1000);
    delay(500);
    noTone(buzzer);
    delay(500);
  }
  else{
    noTone(buzzer);
    
  } 
  
  Serial.println(valorSensor);
   
  delay(1000); 
}